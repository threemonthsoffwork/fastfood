from django.db import models

# Create your models here.
class Order(models.Model):
	customer = models.CharField(max_length=100)
	order = models.CharField(max_length=100)
	created = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ['-created']