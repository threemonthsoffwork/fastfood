from django.shortcuts import render
import urllib.request, json 
import requests
import datetime

def datetime_to_scheduletime(items):
	display_list = []
	time_store = datetime.datetime.combine(datetime.date.today(),datetime.time(0,0,0))
	for count, item in enumerate(reversed(items)):
		display_list.append(str(time_store.time())+' Make '+item['order']+' for '+item['customer'])
		time_store = time_store + datetime.timedelta(minutes = 2.5)
		display_list.append(str(time_store.time())+' Serve '+item['order']+' for '+item['customer'])
		time_store = time_store + datetime.timedelta(minutes = 1)

	display_list.append(str(time_store.time())+' Take a break')
	return display_list


def home_page(request):
	if request.method == 'POST':
		name_var = request.POST["name_text"]
		order_var = request.POST["order_text"]
		requests.post('http://127.0.0.1:8000/api/orders', data = {'customer':name_var, 'order': order_var})

	with urllib.request.urlopen("http://127.0.0.1:8000/api/orders") as url:
		items = json.loads(url.read().decode())
	display_list = datetime_to_scheduletime(items)
	return render(request, 'home.html', {'display_list':display_list})