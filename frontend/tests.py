from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from frontend.views import home_page
from frontend.views import datetime_to_scheduletime
import datetime

# Create your tests here.
class HomePageTest(TestCase):

	def test_root_url_resolves_to_home_page_view(self):
		found = resolve('/')
		self.assertEqual(found.func, home_page)

	def test_home_page_resturns_correct_html(self):
		request = HttpRequest()
		response = home_page(request)
		html = response.content.decode('utf8')
		self.assertTrue(html.startswith('<html>'))
		self.assertIn('<title>FastFood</title>',html)
		self.assertTrue(html.endswith('</html>'))

	def test_datetime_to_scheduletime(self):
		json_list = [{"id":19,"customer":"Stravos","order":"Chicken Sandwich","created":"2022-01-27T11:13:32.231424Z"},{"id":18,"customer":"Stravos","order":"Chicken Sandwich","created":"2022-01-27T11:11:14.434699Z"}]
		display_list  = datetime_to_scheduletime(json_list)
		self.assertEqual(display_list[0], '00:00:00 Make Chicken Sandwich for Stravos')