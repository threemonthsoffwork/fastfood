Requirements
Django==4.0.1
djangorestframework==3.13.1
selenium==4.1.0
requests==2.27.1


How to run
python manage.py makemigrations
python manage.py migrate
python manage.py runserver

How to run unit tests
python manage.py test front_end
python manage.py test orders


How to run functional tests
python manage.py test functional_tests

Pages
HomePage - http://127.0.0.1:8000/ 
DjangoAdmin - http://127.0.0.1:8000/admin
RestAPIAdmin - http://127.0.0.1:8000/api/orders


Description of functionality
The project has two apps
Orders - is an api that handles orders
Frontend - is just some templates that calls the Orders api endpoints to access the database