from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
import time

class NewVisitorTest(LiveServerTestCase):

	def setUp(self):
		self.browser = webdriver.Firefox()

	def tearDown(self):
		self.browser.quit()

	def test_can_start_a_order_and_retrieve_it_later(self):
		# A user visits the website and notices the header mentions FastFood
		self.browser.get(self.live_server_url)	
		self.assertIn('FastFood', self.browser.title)

		# There is a text box inviting the user to make an order
		inputbox1 = self.browser.find_element_by_id('id_new_name')
		inputbox1.send_keys('Stravos')
		inputbox2 = self.browser.find_element_by_id('id_new_order')
		inputbox2.send_keys('Chicken Sandwich')
		inputbox2.send_keys(Keys.ENTER)
		time.sleep(2)

		# The page updates and shows the users order in the queue
		table = self.browser.find_element_by_id('id_order_table')
		rows = table.find_elements_by_tag_name('tr')
		self.assertIn('1: 00:00:00 Make Chicken Sandwich for Stravos', [row.text for row in rows])
		self.assertIn('2: 00:02:30 Serve Chicken Sandwich for Stravos', [row.text for row in rows])

		# A second user enters an order
		inputbox1 = self.browser.find_element_by_id('id_new_name')
		inputbox1.send_keys('Anisa')
		inputbox2 = self.browser.find_element_by_id('id_new_order')
		inputbox2.send_keys('Cheese Sandwich')
		inputbox2.send_keys(Keys.ENTER)
		time.sleep(2)

		# The page updates and shows the second users order in the queue
		table = self.browser.find_element_by_id('id_order_table')
		rows = table.find_elements_by_tag_name('tr')
		self.assertIn('3: 00:03:30 Make Cheese Sandwich for Anisa', [row.text for row in rows])	
		self.assertIn('4: 00:06:00 Serve Cheese Sandwich for Anisa', [row.text for row in rows])


		# A third user enters an order
		inputbox1 = self.browser.find_element_by_id('id_new_name')
		inputbox1.send_keys('Adeel')
		inputbox2 = self.browser.find_element_by_id('id_new_order')
		inputbox2.send_keys('Egg Sandwich')
		inputbox2.send_keys(Keys.ENTER)
		time.sleep(2)

		# The page updates and shows the third users order in the queue
		table = self.browser.find_element_by_id('id_order_table')
		rows = table.find_elements_by_tag_name('tr')
		self.assertIn('5: 00:07:00 Make Egg Sandwich for Adeel', [row.text for row in rows])	
		self.assertIn('6: 00:09:30 Serve Egg Sandwich for Adeel', [row.text for row in rows])

		# As there are no more orders the queue should contain a 'take a break message'
		table = self.browser.find_element_by_id('id_order_table')
		rows = table.find_elements_by_tag_name('tr')
		self.assertIn('7: 00:10:30 Take a break', [row.text for row in rows])		

